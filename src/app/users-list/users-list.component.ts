import { Component, OnInit } from '@angular/core';
import { UsersService } from "../services/users/users.service";
import { User } from "../shared/models/user.model";

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrl: './users-list.component.scss'
})
export class UsersListComponent implements OnInit {
    usersList: User[];

    constructor(public usersService: UsersService) {
    }

    ngOnInit() {
      this.usersService.getUsersList().subscribe((usersList: User[]) => {
        this.usersList = usersList;
      });
    }
}
