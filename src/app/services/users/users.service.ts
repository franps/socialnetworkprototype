import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs";
import { User } from "../../shared/models/user.model";

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  usersList: User[] = [];

  constructor(public httpClient: HttpClient) { }

  getUsersList() {
    return this.httpClient
      .get<User[]>("https://jsonplaceholder.typicode.com/users/")
      .pipe(
        map(usersList => {
          this.usersList = usersList;
          return usersList;
        })
      );
  }
}
